# This module holds all the paths needed for childsplay on a Debian system
import os, sys

try:
    import IamBinary
except ImportError:
    print "No binary"
    BASEDIR = sys.path[0]
    os.chdir(BASEDIR)

DOCDIR = '/usr/share/doc/childsplay'
PYTHONCPDIR = '/usr/share/pyshared/childsplay'
BASEDIR = '/usr/share/pyshared/childsplay'
SHARELIBDATADIR = '/usr/share/childsplay'
ALPHABETDIR = '/usr/share/sp_alphabetsounds'
LOCALEDIR = '/usr/share/locale'
WWWDIR = '/usr/share/childsplay/www/backend'
